# stream_processing_service

This is a job application assignment

it receives a stream of Jsons and handles it while counting the number of &#34;event_type&#34; and number of words in the &#34;data&#34; attribute

it exposes an Http service so access and observes the counts so far.

Stack:
Scala
Play Framework
Akka Streams to stream the exe
ReactiveMongoDB to save the stats

Instaletion Instractions:

1. download MongoDB to the server you cloned this repo
https://www.mongodb.com/download-center#community
*if the commands mongod and mongo don't work on your shell 
*open the location you installed mongoDB and go to the bin directory the exe should be there
	-	after installing mongodb open a console and type mongod (you just started you mongodb server)
2. configure mongodb
	-	open a diffrent shell and type mongo (you just opened a mongo shell) 
	-	type: "use streamStatistics" and press enter (you created a db in mongoDB)
	- 	type: "db.createUser({user:"statsUser", pwd:"123456789", roles:["readWrite"]})" and press enter (you created a user)
if all went well we have a db with a database called "streamStatistics" and a user named statsUser
you can take a look at "PROJECT_LOCATION\stream_processing_service\stream_processing_service\conf\application.conf"
and see that the DB is defined in Play the same way that we just defined it.


3. active play
	-	open another shell
	-	cd PROJECT_LOCATION\stream_processing_service\stream_processing_service
	-	type "sbt run"
play will compile the server and will start listening on localhost:9000


4. test
	-	open a browser
	- you can try one of the exposed apis:
		- localhost:9000/api/getWordCount/:theWordYouWantTheCountFor
		- localhost:9000/api/getWordCount
		- localhost:9000/api/getEventTypeCount/:theEventYouWantTheCountFor
		- localhost:9000/api/getEventTypeCount

you can take a look at the routes file:
PROJECT_LOCATION\stream_processing_service\stream_processing_service\conf\routes
and see the defined apis
