// @GENERATOR:play-routes-compiler
// @SOURCE:C:/ScalaProjects/stream_processing_service/stream_processing_service/conf/routes
// @DATE:Tue May 29 17:39:43 IDT 2018

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset

// @LINE:4
package controllers.javascript {

  // @LINE:7
  class ReverseStreamStatsCtrl(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:10
    def getAllEventsTypeCount: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StreamStatsCtrl.getAllEventsTypeCount",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getEventTypeCount"})
        }
      """
    )
  
    // @LINE:7
    def getWordCount: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StreamStatsCtrl.getWordCount",
      """
        function(word0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getWordCount/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("word", word0))})
        }
      """
    )
  
    // @LINE:9
    def getEventTypeCount: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StreamStatsCtrl.getEventTypeCount",
      """
        function(eventType0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getEventTypeCount/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("eventType", eventType0))})
        }
      """
    )
  
    // @LINE:8
    def getAllWordsCount: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StreamStatsCtrl.getAllWordsCount",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/getWordCount"})
        }
      """
    )
  
  }

  // @LINE:4
  class ReverseStreamProcessServiceCtrl(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:4
    def process: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StreamProcessServiceCtrl.process",
      """
        function(jsonString0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/processStream" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("jsonString", jsonString0)])})
        }
      """
    )
  
  }


}
