// @GENERATOR:play-routes-compiler
// @SOURCE:C:/ScalaProjects/stream_processing_service/stream_processing_service/conf/routes
// @DATE:Tue May 29 17:39:43 IDT 2018

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:4
package controllers {

  // @LINE:7
  class ReverseStreamStatsCtrl(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:10
    def getAllEventsTypeCount(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getEventTypeCount")
    }
  
    // @LINE:7
    def getWordCount(word:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getWordCount/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("word", word)))
    }
  
    // @LINE:9
    def getEventTypeCount(eventType:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getEventTypeCount/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("eventType", eventType)))
    }
  
    // @LINE:8
    def getAllWordsCount(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/getWordCount")
    }
  
  }

  // @LINE:4
  class ReverseStreamProcessServiceCtrl(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:4
    def process(jsonString:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/processStream" + play.core.routing.queryString(List(Some(implicitly[play.api.mvc.QueryStringBindable[String]].unbind("jsonString", jsonString)))))
    }
  
  }


}
