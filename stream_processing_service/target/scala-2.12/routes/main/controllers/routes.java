// @GENERATOR:play-routes-compiler
// @SOURCE:C:/ScalaProjects/stream_processing_service/stream_processing_service/conf/routes
// @DATE:Tue May 29 17:39:43 IDT 2018

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseStreamStatsCtrl StreamStatsCtrl = new controllers.ReverseStreamStatsCtrl(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseStreamProcessServiceCtrl StreamProcessServiceCtrl = new controllers.ReverseStreamProcessServiceCtrl(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseStreamStatsCtrl StreamStatsCtrl = new controllers.javascript.ReverseStreamStatsCtrl(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseStreamProcessServiceCtrl StreamProcessServiceCtrl = new controllers.javascript.ReverseStreamProcessServiceCtrl(RoutesPrefix.byNamePrefix());
  }

}
