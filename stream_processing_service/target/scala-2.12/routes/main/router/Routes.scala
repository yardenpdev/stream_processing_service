// @GENERATOR:play-routes-compiler
// @SOURCE:C:/ScalaProjects/stream_processing_service/stream_processing_service/conf/routes
// @DATE:Tue May 29 17:39:43 IDT 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:4
  StreamProcessServiceCtrl_0: controllers.StreamProcessServiceCtrl,
  // @LINE:7
  StreamStatsCtrl_1: controllers.StreamStatsCtrl,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:4
    StreamProcessServiceCtrl_0: controllers.StreamProcessServiceCtrl,
    // @LINE:7
    StreamStatsCtrl_1: controllers.StreamStatsCtrl
  ) = this(errorHandler, StreamProcessServiceCtrl_0, StreamStatsCtrl_1, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, StreamProcessServiceCtrl_0, StreamStatsCtrl_1, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/processStream""", """controllers.StreamProcessServiceCtrl.process(jsonString:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getWordCount/""" + "$" + """word<[^/]+>""", """controllers.StreamStatsCtrl.getWordCount(word:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getWordCount""", """controllers.StreamStatsCtrl.getAllWordsCount()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getEventTypeCount/""" + "$" + """eventType<[^/]+>""", """controllers.StreamStatsCtrl.getEventTypeCount(eventType:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/getEventTypeCount""", """controllers.StreamStatsCtrl.getAllEventsTypeCount()"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:4
  private[this] lazy val controllers_StreamProcessServiceCtrl_process0_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/processStream")))
  )
  private[this] lazy val controllers_StreamProcessServiceCtrl_process0_invoker = createInvoker(
    StreamProcessServiceCtrl_0.process(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StreamProcessServiceCtrl",
      "process",
      Seq(classOf[String]),
      "POST",
      this.prefix + """api/processStream""",
      """ Stream Processing Service""",
      Seq()
    )
  )

  // @LINE:7
  private[this] lazy val controllers_StreamStatsCtrl_getWordCount1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getWordCount/"), DynamicPart("word", """[^/]+""",true)))
  )
  private[this] lazy val controllers_StreamStatsCtrl_getWordCount1_invoker = createInvoker(
    StreamStatsCtrl_1.getWordCount(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StreamStatsCtrl",
      "getWordCount",
      Seq(classOf[String]),
      "GET",
      this.prefix + """api/getWordCount/""" + "$" + """word<[^/]+>""",
      """ Http exopse of data""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_StreamStatsCtrl_getAllWordsCount2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getWordCount")))
  )
  private[this] lazy val controllers_StreamStatsCtrl_getAllWordsCount2_invoker = createInvoker(
    StreamStatsCtrl_1.getAllWordsCount(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StreamStatsCtrl",
      "getAllWordsCount",
      Nil,
      "GET",
      this.prefix + """api/getWordCount""",
      """""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_StreamStatsCtrl_getEventTypeCount3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getEventTypeCount/"), DynamicPart("eventType", """[^/]+""",true)))
  )
  private[this] lazy val controllers_StreamStatsCtrl_getEventTypeCount3_invoker = createInvoker(
    StreamStatsCtrl_1.getEventTypeCount(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StreamStatsCtrl",
      "getEventTypeCount",
      Seq(classOf[String]),
      "GET",
      this.prefix + """api/getEventTypeCount/""" + "$" + """eventType<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_StreamStatsCtrl_getAllEventsTypeCount4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/getEventTypeCount")))
  )
  private[this] lazy val controllers_StreamStatsCtrl_getAllEventsTypeCount4_invoker = createInvoker(
    StreamStatsCtrl_1.getAllEventsTypeCount(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StreamStatsCtrl",
      "getAllEventsTypeCount",
      Nil,
      "GET",
      this.prefix + """api/getEventTypeCount""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:4
    case controllers_StreamProcessServiceCtrl_process0_route(params@_) =>
      call(params.fromQuery[String]("jsonString", None)) { (jsonString) =>
        controllers_StreamProcessServiceCtrl_process0_invoker.call(StreamProcessServiceCtrl_0.process(jsonString))
      }
  
    // @LINE:7
    case controllers_StreamStatsCtrl_getWordCount1_route(params@_) =>
      call(params.fromPath[String]("word", None)) { (word) =>
        controllers_StreamStatsCtrl_getWordCount1_invoker.call(StreamStatsCtrl_1.getWordCount(word))
      }
  
    // @LINE:8
    case controllers_StreamStatsCtrl_getAllWordsCount2_route(params@_) =>
      call { 
        controllers_StreamStatsCtrl_getAllWordsCount2_invoker.call(StreamStatsCtrl_1.getAllWordsCount())
      }
  
    // @LINE:9
    case controllers_StreamStatsCtrl_getEventTypeCount3_route(params@_) =>
      call(params.fromPath[String]("eventType", None)) { (eventType) =>
        controllers_StreamStatsCtrl_getEventTypeCount3_invoker.call(StreamStatsCtrl_1.getEventTypeCount(eventType))
      }
  
    // @LINE:10
    case controllers_StreamStatsCtrl_getAllEventsTypeCount4_route(params@_) =>
      call { 
        controllers_StreamStatsCtrl_getAllEventsTypeCount4_invoker.call(StreamStatsCtrl_1.getAllEventsTypeCount())
      }
  }
}
