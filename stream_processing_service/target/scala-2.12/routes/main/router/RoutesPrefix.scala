// @GENERATOR:play-routes-compiler
// @SOURCE:C:/ScalaProjects/stream_processing_service/stream_processing_service/conf/routes
// @DATE:Tue May 29 17:39:43 IDT 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
