name := """stream_processing_service"""
organization := "com.bigpanda"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
	guice,
	ws,
	ehcache,
	"org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
	"org.reactivemongo" %% "play2-reactivemongo" % "0.13.0-play26"
)