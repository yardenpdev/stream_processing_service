package streamHelpers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.libs.ws._
import play.api.libs.json.{JsObject, Json}

import akka.stream._
import akka.stream.scaladsl._

import akka.util.ByteString
import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props }

import scala.concurrent.{ ExecutionContext, Future }
import scala.concurrent.ExecutionContext.Implicits.global
import akka.Done

/**
	A Box to hold a process that outputs a stream to the server

	just inject the class and the process will start running
	TODO: make StreamBox a class that can be reused
*/
class StreamBox @Inject()(ws: WSClient) {

	val processPath:String = "..\\generator-windows-amd64.exe"
	Logger.info("creating source for stream:"+processPath)
	val process:Process = new ProcessBuilder(processPath).start();
	val dataSource: Source[ByteString, _] = StreamConverters.fromInputStream(() => process.getInputStream())

	implicit val system: ActorSystem = ActorSystem("streamPorcessingSystem")
	implicit val materializer = ActorMaterializer()
	
	val request:WSRequest = ws.url("http://localhost:9000/api/processStream")
    val doneProcess:Future[Done] = 
    		dataSource
				.map(byteString => byteString.utf8String)
				.runForeach(jsonString => request.addQueryStringParameters("jsonString" -> jsonString)
					.post(EmptyBody))(materializer)

}

