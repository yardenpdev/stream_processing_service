package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.libs.json._
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }

import reactivemongo.api.commands.UpdateWriteResult
import streamHelpers.StreamBox
import dao.StatsDao

/**
  Controller that handel the processing of the stream
  talks with the dao and updates the stats of the stream (wordCount, eventTypeCount)
*/
class StreamProcessServiceCtrl @Inject()(cc: ControllerComponents, streamBox:StreamBox, statsDao:StatsDao) extends AbstractController(cc) {
    
  implicit def ec: ExecutionContext = cc.executionContext

  def validateAndProccessString(jsonBatch:String) = {
   	val jsonList:List[JsValue] = jsonBatch.split("\n").toList.filter(isValidInput(_)).map(line => Json.parse(line))
  	jsonList
  }


  def isValidInput(line:String):Boolean = {
  	line.startsWith("{") && line.endsWith("}") && line.contains("event_type") && line.contains("data")
  }

  def process(jsonString: String) = Action.async { implicit request: Request[AnyContent] =>
  	val jsons:List[JsValue] = validateAndProccessString(jsonString)
  	val words:List[String] = jsons.map(json => (json \ "data").as[String])
  	val eventTypes:List[String] = jsons.map(json => (json \ "event_type").as[String])
  	val updateWriteResultWordsFList:Future[List[UpdateWriteResult]] = Future.sequence(words.map(word => statsDao.incrementWord(word)))
  	val updateWriteResultEventFList:Future[List[UpdateWriteResult]] = Future.sequence(eventTypes.map(eventType => statsDao.incrementEventType(eventType)))
    val writeResultsF:Future[(List[UpdateWriteResult],List[UpdateWriteResult])] = for{
      updateWriteResultWord <- updateWriteResultWordsFList
      updateWriteResultEvent <- updateWriteResultEventFList
    } yield (updateWriteResultWord, updateWriteResultEvent)
	  writeResultsF.map(resListOfTupel => Ok(resListOfTupel.toString()))
  }

}
