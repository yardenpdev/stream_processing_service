package controllers

import javax.inject._
import play.api._
import play.api.mvc._

import scala.concurrent.{ ExecutionContext, Future }
import reactivemongo.play.json._
import reactivemongo.play.json.collection._
import reactivemongo.bson.{BSONDocument, BSONBoolean}

import play.api.libs.json.{JsObject, Json}

import dao.StatsDao

import models.{WordCount, EventTypeCount}
import models.WordCount._

/**
	retrives the stats of the Stream wordsCount and eventType Count
*/
class StreamStatsCtrl @Inject()(cc: ControllerComponents, val statsDao:StatsDao) 
		extends AbstractController(cc) {

	implicit def ec: ExecutionContext = cc.executionContext

 	def getWordCount(word:String) = Action.async { implicit request: Request[AnyContent] =>
			val wordCountF:Future[Option[WordCount]] = statsDao.getWordCount(word)
			wordCountF.map(wordCount => 
				Ok(Json.toJson[WordCount](wordCount.getOrElse(WordCount(word,0))))
				)
	}

	def getAllWordsCount() = Action.async { implicit request: Request[AnyContent] => 
		val wordCountListF:Future[List[Option[WordCount]]] = statsDao.getAllWordDocs()
		wordCountListF.map(wordListCountOpt => wordListCountOpt.flatMap(wordCount => wordCount))
					  .map(wordListCount => Ok(Json.toJson[Seq[WordCount]](wordListCount)))
	}

	def getEventTypeCount(eventType:String) = Action.async { implicit request: Request[AnyContent] => 
		val eventTypeCountF:Future[Option[EventTypeCount]] = statsDao.getEventTypeCount(eventType)
		eventTypeCountF.map(eventTypeCount => 
			Ok(Json.toJson[EventTypeCount](eventTypeCount.getOrElse(EventTypeCount(eventType,0))))
			)
	}

	def getAllEventsTypeCount() = Action.async { implicit request: Request[AnyContent] => 
		val eventsCountListF:Future[List[Option[EventTypeCount]]] = statsDao.getAllEventTypeDocs()
		eventsCountListF.map(eventsCountListOpt => eventsCountListOpt.flatMap(eventCount => eventCount))
						.map(eventsCountList => Ok(Json.toJson[Seq[EventTypeCount]](eventsCountList)))
	}	
}
