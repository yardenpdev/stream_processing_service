package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class WordCount(word:String, count:Int)

object WordCount {
	implicit val wordCountWrites: Writes[WordCount] = (
	  (JsPath \ "word").write[String] and
	  (JsPath \ "count").write[Int]
	)(unlift(WordCount.unapply))

	implicit val wordCountReads: Reads[WordCount] = (
	  (JsPath \ "word").read[String] and
	  (JsPath \ "count").read[Int]
	)(WordCount.apply _)
}
