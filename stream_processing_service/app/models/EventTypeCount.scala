package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class EventTypeCount(eventType:String, count:Int)

object EventTypeCount {
	implicit val eventTypeCountWrites: Writes[EventTypeCount] = (
	  (JsPath \ "eventType").write[String] and
	  (JsPath \ "count").write[Int]
	)(unlift(EventTypeCount.unapply))

	implicit val eventTypeCountReads: Reads[EventTypeCount] = (
	  (JsPath \ "eventType").read[String] and
	  (JsPath \ "count").read[Int]
	)(EventTypeCount.apply _)
}

