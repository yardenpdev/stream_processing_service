package dao

import javax.inject._
import play.api._
import play.api.mvc._
import play.modules.reactivemongo.{
  MongoController,
  ReactiveMongoApi,
  ReactiveMongoComponents
}
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{Failure, Success}
import reactivemongo.play.json._
import reactivemongo.play.json.collection._
import reactivemongo.bson.{BSONDocument, BSONBoolean}
import reactivemongo.api.commands.UpdateWriteResult
import reactivemongo.api.Cursor
import reactivemongo.bson.BSONObjectID
import play.api.libs.json._

import models.EventTypeCount
import models.WordCount

/**
	Data access object to mongoDB to the collections:
	eventsTypeCount and wordsCount
*/

@Singleton
class StatsDao @Inject()(cc: ControllerComponents, val reactiveMongoApi: ReactiveMongoApi) 
	extends AbstractController(cc) with MongoController with ReactiveMongoComponents {

	implicit def ec: ExecutionContext = cc.executionContext

	   def collectionEventsTypeCount: Future[JSONCollection] = reactiveMongoApi.database.map(defualtDB => defualtDB.collection[JSONCollection]("eventTypeCount"))
   	def collectionWordCount: Future[JSONCollection] = reactiveMongoApi.database.map(defualtDB => defualtDB.collection[JSONCollection]("wordsCount"))

      /**  
         a word is found in the DB or not found, if not found then the count is 0
      */
   	def getWordCount(word:String):Future[Option[WordCount]] = {
   		val selector = BSONDocument("word" -> word)
   		val listCollectedF:Future[List[JsObject]] = collectionWordCount.flatMap(col =>
   			col.find(selector)
   			.cursor[JsObject]()
   			.collect[List](10, Cursor.FailOnError[List[JsObject]]()))
   		listCollectedF.map(list => {
            list.size match {
               case 1 => Some(list(0))
               case size if size > 1 => { groupByDocuments(word, list, collectionWordCount, incrementWord); 
                                          Some(unitListObject("word", list)) } 
               case _ => None
            }
   		})
         .map(jsObjectOption => {
            jsObjectOption match {
               case Some(jsObject: JsObject) => {
                  val jsResult:JsResult[WordCount] = jsObject.validate[WordCount]
                  jsResult match {
                      case s: JsSuccess[WordCount] => s.asOpt
                      case e: JsError => { Logger.error("Errors: " + JsError.toJson(e).toString());  None }
                   }
               }
               case _ => None
            }
         })
   	}

   	def getAllWordDocs():Future[List[Option[WordCount]]] = {
   		val exclude = BSONDocument("_id" -> 0)
   		collectionWordCount.flatMap(col =>
			col.find(Json.obj(),exclude)
			.cursor[JsObject]()
			.collect[List](-1, Cursor.FailOnError[List[JsObject]]()))
         .map(jsObjectList =>
            jsObjectList.map(jsObject => {
            val jsResult:JsResult[WordCount] = jsObject.validate[WordCount]
            jsResult match {
                case s: JsSuccess[WordCount] => s.asOpt
                case e: JsError => { Logger.error("Errors: " + JsError.toJson(e).toString());  None }
             }
         }))
   	}

   	def getEventTypeCount(eventType:String):Future[Option[EventTypeCount]] = {
   		val selector = BSONDocument("eventType" -> eventType)
   		val listCollectedF:Future[List[JsObject]] = collectionEventsTypeCount.flatMap(col =>
   			col.find(selector)
   			.cursor[JsObject]()
   			.collect[List](10, Cursor.FailOnError[List[JsObject]]()))
         listCollectedF.map(list => {
            list.size match {
               case 1 => Some(list(0))
               case size if size > 1 => { groupByDocuments(eventType, list, collectionEventsTypeCount, incrementEventType); 
                                          Some(unitListObject("eventType", list)) } 
               case _ => None
            }
         })
         .map(jsObjectOption => {
            jsObjectOption match {
               case Some(jsObject: JsObject) => {
                  val jsResult:JsResult[EventTypeCount] = jsObject.validate[EventTypeCount]
                  jsResult match {
                      case s: JsSuccess[EventTypeCount] => s.asOpt
                      case e: JsError => { Logger.error("Errors: " + JsError.toJson(e).toString());  None }
                   }
               }
               case _ => None
            }
         })
   	}

   	def getAllEventTypeDocs():Future[List[Option[EventTypeCount]]] = {
   		val exclude = BSONDocument("_id" -> 0)
   		collectionEventsTypeCount.flatMap(col => 
			col.find(Json.obj(), exclude)
			.cursor[JsObject]()
			.collect[List](-1, Cursor.FailOnError[List[JsObject]]()))
         .map(jsObjectList =>
            jsObjectList.map(jsObject => {
               val jsResult:JsResult[EventTypeCount] = jsObject.validate[EventTypeCount]
               jsResult match {
                   case s: JsSuccess[EventTypeCount] => s.asOpt
                   case e: JsError => { Logger.error("Errors: " + JsError.toJson(e).toString());  None }
               }
            })
         )
   	}

   	def incrementWord(word:String):Future[UpdateWriteResult] = {
   		val selector = BSONDocument("word" -> word)
   		val toUpdate = BSONDocument("$inc" -> BSONDocument("count" -> 1))
   		collectionWordCount.flatMap(col =>
   			col.update(selector, toUpdate, upsert = true))
   	}


   	def incrementEventType(eventType:String):Future[UpdateWriteResult] = {
   		val selector = BSONDocument("eventType" -> eventType)
   		val toUpdate = BSONDocument("$inc" -> BSONDocument("count" -> 1))
   		collectionEventsTypeCount.flatMap(col =>
   			col.update(selector, toUpdate, upsert = true))
   	}

      /**
         groups documents that were created by 2 diffrent threads 
         and increment the bigger document
      */
      private def groupByDocuments(valueToInc:String, list:List[JsObject], colF:Future[JSONCollection], incFunc:(String) => Unit) = {
         list.map(doc => {
            if((doc \ "count").as[Int].intValue == 1) {
               val id = (doc \ "_id").as[BSONObjectID]
               val selector = BSONDocument("_id" -> id)
               val writeResultF = colF.map(col => col.remove(selector))
               writeResultF.onComplete {
                  case Success(writeResult) => incFunc(valueToInc)
                  case Failure(e) => e.printStackTrace()
               }
            }
         })    
      }

      /**
            this function aggragates the stats of a stats type
            * this function is here due to a first insert to mongo that might results a few docs with the same name 
            the list is assumed to hold JsObject of the same stats type (wordCount/eventTypeCount) 
            the list size is assumed to be bigger then 1
            TODO - assert constrants on function
      */
      private def unitListObject(statsType:String, list:List[JsObject]):JsObject = {
         if(list.size <= 1)
            throw new IllegalArgumentException("list size is assumed to be bigger then 1")
         val count = list
                     .map(jsObject => (jsObject \ "count").as[Int])
                     .foldLeft(0)(_ + _)
         val stasName = (list(0) \ statsType).as[String]
         val jsObjectRes = JsObject(Seq(statsType -> JsString(stasName), "count" -> JsNumber(count)))
         jsObjectRes
      }
}